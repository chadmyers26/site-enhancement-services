$( document ).ready(function() {
	
	$('.bxslider').bxSlider({
		auto: true,
		mode: 'vertical',
		responsive: true,
		pager: false,
		controls: false,
		pause: 5000
	});

	$('a[href^="#"]').on('click', function(event) {

	    var target = $(this.getAttribute('href'));

	    if( target.length ) {
	        event.preventDefault();
	        $('html, body').stop().animate({
	            scrollTop: target.offset().top - 131
	        }, 1000);
	    }

	});


	$('.about-main-photo').on('click', function() {
	    $(this).html('<video id="ses-about-video" autoplay controls><source src="video/ses-people-video.mp4"></video>').css('background', 'none');
	
		setTimeout(function(){ 
			document.getElementById('ses-about-video').addEventListener('ended',myHandler,false);

		    function myHandler(e) {
		        $(".about-main-photo").html('<img class="about-ses-video-play-img" src="img/ses-overview-video-play.jpg">');		
		    } 
		}, 3000);
	});

	$('.privacy').on('click', function() {
	    $("#privacyModal").css("display", "block");
	});

	$('.copyright').on('click', function() {
	    $("#copyrightModal").css("display", "block");
	});

	$("#privacyModal").find(".close").on('click', function() {
	  $("#privacyModal").css("display", "none");
	});

	$("#copyrightModal").find(".close").on('click', function() {
	  $("#copyrightModal").css("display", "none");
	});

	window.onclick = function(event) {
		if (event.target.id == "copyrightModal") {
			$("#copyrightModal").css("display", "none");
		} else if (event.target.id == "privacyModal") {
			$("#privacyModal").css("display", "none");
		}
	}
});