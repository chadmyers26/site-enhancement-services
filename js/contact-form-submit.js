$( document ).ready(function() {
	
	$( "#contact-form" ).submit(function( event ) {
		event.preventDefault();
		var name = $("#name").val();
		var email = $("#email").val();
		var phone = $("#phone").val();
		var company = $("#company").val();
				
		if(name==''||email==''||phone=='') {
			$(".alert h4").html("Please Fill All Fields");
		} else {
			// AJAX Code To Submit Form.
			$.ajax({
				type: "POST",
				url: "form-to-email.php",
				data: $('#contact-form').serialize(),
				success: function(msg) {
					console.log("Form Submitted: " + msg);
					$( "#contact-form" ).css("display", "none");
					$(".alert h4").html("Thank you for submitting our contact form. We will be in touch soon.");
				}
			});
		}

		return false;
	});

});